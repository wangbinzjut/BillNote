import * as echarts from '../../ec-canvas/echarts';
import * as utils from '../utils';

const app = getApp();
let chart = null;

function initChart(canvas, width, height) {
    chart = echarts.init(canvas, null, {
        width: width,
        height: height
    });
    canvas.setChart(chart);

    let pieOption = {
        backgroundColor: "#ffffff",
        color: ["#e54d42", "#39b54a", "#1cbbb4", "#0081ff", "#6739b6", 
        "#9c26b0", "#e03997", "#a5673f", "#8799a3"],
        series: [{
            label: {
                normal: {
                    formatter: '{b}\r\n￥{c}',
                }
            },
            type: 'pie',
            center: ['50%', '50%'],
            radius: ['25%', '60%'],
            data: [],
        }]
    };

    chart.setOption(pieOption);
    return chart;
}

Page({
    /**
     * 页面的初始数据
     */
    data: {
        curMonthIncome: 0,      // 当前月份总收入
        curMonthExpand: 0,      // 当前月份总支出
        curBillCnt: 0,          // 当前账单个数
        selectcategory: 2,      // 1选中收入，2选中支出
        selectstatistics: 1,    // 1选中分类，2选中月份
        ec: {
            onInit: initChart
        },
        curBillDetail: [],      // 账本对应的详细账单
        incomeDetail: [],       // 收入详情
        expandDetail: [],       // 支出详情
        curMonth: (new Date()).Format("yyyy-MM"), // 账本名称，当前月份
        curYearValue: (new Date()).Format("yyyy"),
        curMonthValue: (new Date()).Format("MM"),
        pieOption: {
            backgroundColor: "#ffffff",
            color: ["#e54d42", "#39b54a", "#1cbbb4", "#0081ff", "#6739b6", 
            "#9c26b0", "#e03997", "#a5673f", "#8799a3"],
            series: [{
                label: {
                    normal: {
                        formatter: '{b}\r\n￥{c}',
                    }
                },
                type: 'pie',
                center: ['50%', '50%'],
                radius: ['25%', '60%'],
                data: [],
            }]
        },
        barOption: {
            color: ['#3398DB'],
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: [],
                axisTick: {
                    alignWithLabel: true
                }
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                data: [],
                type: 'bar',
                label: {
                    normal: {
                        show: true,
                        position: 'top'
                    }
                },
                barWidth: '60%',
            }]
        },
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
    },

    onShow() {
        try {
            let timestamp = 0;
            timestamp = wx.getStorageSync("getBillList")
            if (timestamp > 0) {
                wx.removeStorageSync("getBillList");
                return;
            }
        } catch (e) {
            console.log("no getBillList value");
        }
        console.log("onshow")
        this.loadCurMonthBill(0);
    },

    resetChartOption(option) {
        chart.setOption(option);
    },

    echartInit(e) {
        this.initChart(e.detail.canvas, e.detail.width, e.detail.height);
    },

    // 加载当前选择月份的账单
    loadCurMonthBill(skipIndex) {
        let that = this;
        const db = wx.cloud.database();
        const _ = db.command;
        const $ = db.command.aggregate;
        if (0 == skipIndex) {
            this.data.curBillDetail = [];
            wx.showLoading({
                title: '数据加载中',
                mask: true,
            });
        }

        db.collection("bills").where({
            "account": that.data.curMonth
        }).orderBy("date", "desc").skip(skipIndex).get().then(res => {
            res.data.forEach(element => {
                that.data.curBillDetail.push(element);
            });
            skipIndex += res.data.length;
            if (0 == res.data.length) {
                that.setData({
                    curBillDetail: this.data.curBillDetail,
                })
                wx.hideLoading();
                that.calcReloadChart();
            } else {
                that.loadCurMonthBill(skipIndex);
            }
        });
    },

    /**
     * 计算当月收入和支出
     */
    calcReloadChart() {
        chart.clear();
        let actDetail = this.data.curBillDetail;
        this.data.curMonthIncome = 0;
        this.data.curMonthExpand = 0;
        this.data.curBillCnt = 0;
        let expandData = {};
        let incomeData = {};
        this.data.incomeDetail = [];
        this.data.expandDetail = [];

        let incomeCnt = 0;
        let expandCnt = 0;
        // 计算总收入和总支出
        actDetail.forEach(element => {
            if ("收入" == element.category) {
                incomeCnt++;
                this.data.curMonthIncome += element.money;
                if (!incomeData[element.itemtext]) {
                    incomeData[element.itemtext] = element.money;
                } else {
                    incomeData[element.itemtext] += element.money;
                }
                this.data.incomeDetail.push(element);
            } else {
                expandCnt++;
                this.data.curMonthExpand += element.money;
                if (!expandData[element.itemtext]) {
                    expandData[element.itemtext] = element.money;
                } else {
                    expandData[element.itemtext] += element.money;
                }
                this.data.expandDetail.push(element);
            }
        });

        if (1 == this.data.selectstatistics) {          // 按分类展示饼图
            this.data.pieOption.series[0].data = [];
            if (1 == this.data.selectcategory) {
                this.data.curBillCnt = incomeCnt;
                for (var key in incomeData) {
                    var obj = {};
                    obj.name = key;
                    obj.value = incomeData[key].toFixed(2);
                    if (1 == this.data.selectstatistics) this.data.pieOption.series[0].data.push(obj);
                }
            } else if (2 == this.data.selectcategory) {
                this.data.curBillCnt = expandCnt;
                for (var key in expandData) {
                    var obj = {};
                    obj.name = key;
                    obj.value = expandData[key].toFixed(2);
                    if (1 == this.data.selectstatistics) this.data.pieOption.series[0].data.push(obj);
                }
            }
            this.setData({
                curBillCnt: this.data.curBillCnt,
            });
            this.resetChartOption(this.data.pieOption);
        } else if (2 == this.data.selectstatistics) {   // 按月份展示柱状图
            this.data.barOption.series[0].data = [];
            this.data.barOption.xAxis.data = [];

            let monthList = utils.getLatestYearMonth(new Date(this.data.curMonth), 6).reverse();
            monthList.forEach(element => {
                this.data.barOption.xAxis.data.push(element.substring(5,7) + "月");
            });
            let minMonth = monthList[0];
            let maxMonth = monthList[5];

            // 获取近6个月的数据
            let that = this;
            const db = wx.cloud.database();
            const _ = db.command;
            const $ = db.command.aggregate;

            let category = "支出";
            if (1 == that.data.selectcategory) {
                category = "收入"
            }
            
            console.log(monthList[0]);
            console.log(monthList[5]);
            db.collection("bills").aggregate().match({
                account: _.gte(monthList[0]).and(_.lte(monthList[5])),
                category: _.eq(category)
            }).group({
                _id: {
                    account: '$account',
                },
                totalMoney: $.sum('$money')
            }).end().then(res => {
                let moneyData = {};
                res.list.forEach(element => {
                    moneyData[element._id.account] = element.totalMoney;
                });

                monthList.forEach(e => {
                    this.data.barOption.series[0].data.push(moneyData[e] != undefined ?moneyData[e] : 0);
                });

                wx.hideLoading();
                this.resetChartOption(this.data.barOption);
            })
        }
        this.setData({
            curMonthIncome: this.data.curMonthIncome,
            curMonthExpand: this.data.curMonthExpand,
            incomeDetail: this.data.incomeDetail,
            expandDetail: this.data.expandDetail,
        });
    },

    // 收入/支出
    onSelectCategory(e) {
        if (this.data.selectcategory != e.currentTarget.dataset.index) {
            this.setData({
                selectcategory: e.currentTarget.dataset.index
            });
            this.calcReloadChart();
        }
    },

    // 选择分类/月份，按分类或月份展示图表
    onSelectStatistics(e) {
        this.setData({
            selectstatistics: e.currentTarget.dataset.i
        });
        this.calcReloadChart();
    },

    onYearChange(e) {
        this.data.curYearValue = e.detail;
    },

    onMonthChange(e) {
        this.data.curMonthValue = e.detail;
    },

    // 选择年和月之后确定
    onSelectYearMonth() {
        let m = parseInt(this.data.curMonthValue) > 9 ? this.data.curMonthValue : "0" + this.data.curMonthValue;
        this.setData({
            curMonth: this.data.curYearValue + "-" + m
        });
        console.log(this.data.curMonth);
        this.loadCurMonthBill(0);
    },
})