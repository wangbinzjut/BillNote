import * as utils from '../utils';

const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo: {
            avatarUrl: "/images/user-unlogin.png"
        },
        totalIncome: 0,
        totalExpand: 0,
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if (app.globalData.userInfo) {
            this.setData({
                userInfo: app.globalData.userInfo,
                hasUserInfo: true
            })
        } else if (this.data.canIUse) {
            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
            // 所以此处加入 callback 以防止这种情况
            app.userInfoReadyCallback = res => {
                this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                })
            }
        } else {
            // 在没有 open-type=getUserInfo 版本的兼容处理
            wx.getUserInfo({
                success: res => {
                    app.globalData.userInfo = res.userInfo
                    this.setData({
                        userInfo: res.userInfo,
                        hasUserInfo: true
                    })
                }
            })
        }
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        try {
            let timestamp = 0;
            timestamp = wx.getStorageSync("goHisList")
            if (timestamp > 0) {
                wx.removeStorageSync("goHisList");
                return;
            }
        } catch (e) {
            console.log("no goHisList value");
        }
        let that = this;
        wx.showLoading({
            title: '数据加载中',
            mask: true,
        });
        const db = wx.cloud.database();
        const _ = db.command;
        const $ = db.command.aggregate;
        db.collection("bills").aggregate().group({
            _id: '$category',
            totalMoney: $.sum('$money')
        }).end().then(res => {
            that.data.totalIncome = 0;
            that.data.totalExpand = 0;
            res.list.forEach(element => {
                if (element._id == "收入") {
                    that.data.totalIncome = utils.ToFixed(element.totalMoney)
                } else if (element._id == "支出") {
                    that.data.totalExpand = utils.ToFixed(element.totalMoney)
                }
            });
            that.setData({
                totalIncome: that.data.totalIncome,
                totalExpand: that.data.totalExpand
            });
            wx.hideLoading();
        });
    },

    onShareAppMessage() {
        return {
            title: "简单好用的记账软件",
            imageUrl: "/images/share.jpg"
        }
    },

    goHisList() {
        let timestamp = new Date().getTime();
        wx.setStorageSync("goHisList", timestamp);
        console.log("set time=" + timestamp);
        wx.navigateTo({
            url: '/pages/hislist/hislist',
        })
    },

    bindGetUserInfo(res) {
        if (res.detail.userInfo) {
            app.globalData.userInfo = res.detail.userInfo;
            this.setData({
                userInfo: app.globalData.userInfo,
                hasUserInfo: true
            })
        } else {
            console.log("点击了拒绝授权");
        }
    },
})