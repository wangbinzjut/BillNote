// miniprogram/pages/billlist/billlist.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        billDetail: {},
        checked: 1,
        sortText: '按时间排序',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let timestamp = new Date().getTime();
        wx.setStorageSync("getBillList", timestamp);
        var json = JSON.parse(options.json);
        this.setData({
            billDetail: json
        });
    },

    showModal(e) {
        this.setData({
            modalName: e.currentTarget.dataset.target
        })
    },

    hideModal(e) {
        this.setData({
            modalName: null
        })
    },

    sortBy(filed, rev) {
        rev = (rev) ? -1 : 1;
        return function (a, b) {
            a = a[filed];
            b = b[filed];
            if (a < b) { return rev * -1; }
            if (a > b) { return rev * 1; }
            return 1;
        }
    },

    gridchange(e) {
        let text = "按时间排序";
        // 数据排序
        if (1 == e.detail.value) {
            this.data.billDetail = this.data.billDetail.sort(this.sortBy('date', true));
        } else {
            text = "按金额排序";
            this.data.billDetail = this.data.billDetail.sort(this.sortBy('money', true));
        }

        this.setData({
            billDetail: this.data.billDetail,
            modalName: null,
            sortText: text,
        });
    },
})