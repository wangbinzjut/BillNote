import * as utils from '../utils';

Page({
    /**
     * 页面的初始数据
     */
    data: {
        skipIndex: 0,       // 用于分批获取数据
        billDetail: [],     // 总的账单数据
        haveData: true,    // 下拉是否还有数据
        startDate: (new Date()).Format("yyyy-MM-01"),
        endDate: (new Date()).Format("yyyy-MM-dd"),

        curDate: new Date().getTime(),
        minDate: new Date(2015, 0, 1).getTime(),
        maxDate: new Date().getTime(),

        selectDateType: 1,      // 1为选择开始时间，2为选择结束时间
        dateRangeHeight: 0,     // 选择日期view的高度
        show: {
            bottom: false,
        },
        
        formatter(type, value) {
            if (type === 'year') {
                return `${value}年`;
            } else if (type === 'month') {
                return `${value}月`;
            }
            return value;
        },
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that = this;
        const query = wx.createSelectorQuery();
        query.select('.dateRange').boundingClientRect();
        query.exec(function(res) {
            that.setData({
                dateRangeHeight: res[0].height
            });
        })

        this.loadRemoteBill();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        if (this.data.haveData) {
            this.loadRemoteBill();
        }
    },

    loadRemoteBill() {
        wx.showLoading({
            title: '数据加载中',
            mask: true,
        });
        let that = this;
        const db = wx.cloud.database();
        const _ = db.command
        db.collection("bills").where({
            "date": _.gte(this.data.startDate).and(_.lte(this.data.endDate)),
        }).orderBy("date", "desc").skip(this.data.skipIndex).get().then(res => {
            if (res.data.length == 0) {
                this.data.haveData = false;
            }
            res.data.forEach(element => {
                this.data.billDetail.push(element);
            });
            this.data.skipIndex += res.data.length;
            this.setData({
                billDetail: this.data.billDetail,
            })
            wx.hideLoading();
        });
    },

    // 打开或关闭popup
    toggle(type) {
        this.setData({
            [`show.${type}`]: !this.data.show[type]
        });
    },

    toggleBottomPopup(e) {
        this.toggle('bottom');
        if (this.data.show['bottom']) {
            this.setData({
                selectDateType: e.currentTarget.dataset.i
            });
        }
    },
    
    // 选择月份确定
    onConfirm(event) {
        const { detail, currentTarget } = event;
        const result = utils.GetFormateDate(detail, currentTarget.dataset.type);
        this.toggleBottomPopup();
        if (1 == this.data.selectDateType) {
            this.setData({
                startDate: result
            });
        } else {
            this.setData({
                endDate: result
            });
        }
    },

    // 选择取消
    onCancel(e) {
        this.toggleBottomPopup();
    },

    onConfirmSelect() {
        this.data.haveData = true;
        this.data.billDetail = [];  // 清空之前的记录
        this.data.skipIndex = 0;
        this.loadRemoteBill();
    },

})