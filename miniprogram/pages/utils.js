export let EXPAND = 0;     // 支出
export let INCOME = 1;     // 收入

//导出函数
// input:2019-08-21, output:2019-08
export function getTheMonth(d) {
    return d.substring(0, 7);
}

// input:2019-08-21, output:08-21
export function getTheDate(d) {
    return d.substring(5, 9);
}

// 获取传入date之前的n个月份，返回[2019-08,2019-07,2019-06,2019-05,2019-04,2019-03]
export function getLatestYearMonth(date, n) {
    let datelist = []
    //let date = new Date()
    let Y = date.getFullYear()
    let M = date.getMonth() + 1
    for (let i = 0; i < n; i++) {
        let dateoption = ''
        if (M !== 0) {
        } else {
            M = 12
            Y = Y - 1
        }
        let m = M
        m = m < 10 ? '0' + m : m
        dateoption = Y + '-' + m
        M--
        datelist.push(dateoption)
    }
    return datelist
}

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, // 月份
        "d+": this.getDate(), // 日
        "h+": this.getHours(), // 小时
        "m+": this.getMinutes(), // 分
        "s+": this.getSeconds(), // 秒
        "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
        "S": this.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

// 格式化选择的日期
export function GetFormateDate(time, type) {
    const date = new Date(time);
    switch (type) {
        case 'datetime':
            return date.toLocaleString();
        case 'date':
            return date.Format("yyyy-MM-dd");
        case 'year-month':
            return date.Format("yyyy-MM");
        case 'time':
            return time;
        default:
            return '';
    }
}

export function ToFixed(n) {
    var f = parseFloat(n);
    return f.toFixed(2);
}