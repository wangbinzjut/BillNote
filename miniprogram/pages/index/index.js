import * as utils from "../utils";

const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        StatusBar: app.globalData.StatusBar,
        CustomBar: app.globalData.CustomBar,

        // 支出类别
        expanditem: [
            {"img": "../../images/i-yiban.png", "text": "一般"},
            {"img": "../../images/i-canyin.png", "text": "餐饮"},
            {"img": "../../images/i-yinliao.png", "text": "饮料"},
            {"img": "../../images/i-yiwu.png", "text": "衣物"},
            {"img": "../../images/i-fangwu.png", "text": "房屋"},
            {"img": "../../images/i-gouwu.png", "text": "购物"},
            {"img": "../../images/i-jiaotong.png", "text": "交通"},
            {"img": "../../images/i-shuidian.png", "text": "水电"},
            {"img": "../../images/i-yiliao.png", "text": "医疗"},
            {"img": "../../images/i-dianying.png", "text": "电影"},
            {"img": "../../images/i-tongxun.png", "text": "通讯"},
            {"img": "../../images/i-jiaoyu.png", "text": "教育"},
        ],
        
        // 收入类别
        incomeitem: [
            {"img": "../../images/i-gongzi.png", "text": "工资"},
            {"img": "../../images/i-jiangjin.png", "text": "奖金"},
            {"img": "../../images/i-baoxiao.png", "text": "报销"},
            {"img": "../../images/i-hongbao.png", "text": "红包"},
            {"img": "../../images/i-zhuanzhang.png", "text": "转账"},
            {"img": "../../images/i-qita.png", "text": "其他"},
        ],

        selectExpandItemIndex: 0,
        selectIncomeItemIndex: 0,

        category: ['支出', '收入'],
        TabCur: 0,  // 0表示支出，1表示收入

        money: "",   // 输入的金额
        pickerdate: (new Date()).Format("yyyy-MM-dd"),  // 选择的日期
        note: '',       // 输入的备注信息

        accountName: (new Date()).Format("yyyy-MM"), // 账本名称
        accountDetail: {},      // 账本对应的详细账单
        buttonText: "记一笔",    // 记一笔/修改
        modifyBillId: "",

        localKey: "localBillKey",   // 本地存储数据的key
        curMonthIncome: 0,      // 当月收入
        curMonthExpand: 0,      // 当月支出
        
        show: false,
        actions: [],
    },

    onLoad() {
        try {
            var value = wx.getStorageSync(this.data.localKey);
            if (value) {
                this.setData({
                    accountDetail: value
                });
                this.calcCurIncomeAndExpand();
            }
        } catch (e) {
            console.log("onload getdata error");
        }

        this.loadCurMonthBill();

        // 获取近6个月的月份
        let monthList = utils.getLatestYearMonth(new Date(), 6);
        monthList.forEach(element => {
            let obj = {};
            obj.name = element;
            this.data.actions.push(obj);
        });
        this.setData({
            actions: this.data.actions
        });
    },

    loadBill(skipIndex) {
        let that = this;
        const db = wx.cloud.database();
        const $ = db.command.aggregate
        db.collection("bills").where({
            "account": that.data.accountName,
        }).orderBy("date", "desc").skip(skipIndex).get().then(res => {
            for (let i = 0; i < res.data.length; i++)
            {
                if (!that.data.accountDetail[res.data[i].date])
                {
                    that.data.accountDetail[res.data[i].date] = new Array();
                }
                that.data.accountDetail[res.data[i].date].push(res.data[i]);
            }
            skipIndex += res.data.length;
            if (res.data.length < 20) {
                that.setData({
                    accountDetail: that.data.accountDetail
                })
                wx.setStorage({
                    key: that.data.localKey,
                    data: that.data.accountDetail,
                });
                that.calcCurIncomeAndExpand();
                wx.hideLoading();
                return;
            } else {
                that.loadBill(skipIndex);
            }
        });
    },

    // 加载当前月份账单
    loadCurMonthBill() {
        this.data.accountDetail = {};
        wx.showLoading({
            title: '数据加载中',
            mask: true,
        });
        this.loadBill(0);
    },

    showModal(e) {
        this.setData({
            TabCur: 0,
            money: "",
            pickerdate: (new Date()).Format("yyyy-MM-dd"),
            note: '',
            modalName: e.currentTarget.dataset.target,
            buttonText: "记一笔",
        });
    },

    hideModal(e) {
        this.setData({
            modalName: null
        })
    },

    sortBillByDate(billJson) {
        var arrKey = [];
        var retJson = {}
        for (var val in billJson) {
            arrKey.push(val);
        }
        arrKey.sort();
        arrKey.reverse();
        for (var val in arrKey) {
            retJson[arrKey[val]] = billJson[arrKey[val]];
        }
        return retJson;
    },

    updateBill(billid, modifyData) {
        var that = this;
        let actDetail = this.data.accountDetail;
        for (var datekey in actDetail) {
            let valueArr = actDetail[datekey];
            for (var index in valueArr) {
                if (billid == valueArr[index]._id) {
                    if (modifyData.date == valueArr[index].date) {
                        valueArr[index].account = modifyData.account;
                        valueArr[index].category = modifyData.category;
                        valueArr[index].itemimg = modifyData.itemimg;
                        valueArr[index].itemtext = modifyData.itemtext;
                        valueArr[index].money = modifyData.money;
                        valueArr[index].date = modifyData.date;
                        valueArr[index].note = modifyData.note;
                    } else {    // 日期改变了
                        // 从原来的日期中删除
                        valueArr.splice(index, 1);
                        if (0 == valueArr.length) {
                            delete this.data.accountDetail[datekey];
                        }
                        let add = false;
                        // 在新的日期中新增
                        if (!that.data.accountDetail[modifyData.date])
                        {
                            // 是当前的月份
                            if (utils.getTheMonth(modifyData.date) == this.data.accountName) {
                                add = true;
                                that.data.accountDetail[modifyData.date] = new Array();
                            }
                        }
                        modifyData._id = billid;
                        if (add) {
                            that.data.accountDetail[modifyData.date].push(modifyData);
                        }
                    }
                    that.data.accountDetail = that.sortBillByDate(actDetail);
                    that.setData({
                        accountDetail: that.data.accountDetail
                    });
                    this.calcCurIncomeAndExpand();
                    return;
                }
            }
        }
    },

    formSubmit(e) {
        var that = this;
        const db = wx.cloud.database();
        const bills = db.collection("bills");
        var billitem = that.data.expanditem[that.data.selectExpandItemIndex];
        if (1 == this.data.TabCur) billitem = that.data.incomeitem[that.data.selectIncomeItemIndex];
        let submitData = {
            "account": utils.getTheMonth(e.detail.value.frm_date),
            "category": that.data.category[that.data.TabCur],
            "itemimg": billitem.img,
            "itemtext": billitem.text,
            "money": parseFloat(e.detail.value.frm_money.trim()),
            "date": e.detail.value.frm_date,
            "note": e.detail.value.frm_note,
            "attime": new Date()
        };

        if (e.detail.value.frm_money.trim().length == 0) {
            wx.showToast({
                title: '请输入金额',
                icon: 'none'
            })
            return false;
        }

        if (isNaN(e.detail.value.frm_money)) {
            wx.showToast({
                title: '金额请输入数字',
                icon: 'none'
            })
            return false;
        }

        if (this.data.buttonText == "记一笔") {
            bills.add({
                // data 字段表示需新增的 JSON 数据
                data: submitData,
                success: function (res) {
                    if (submitData.account == that.data.accountName) {
                        submitData._id = res._id;
                        if (!that.data.accountDetail[submitData.date])
                        {
                            that.data.accountDetail[submitData.date] = new Array();
                        }
                        that.data.accountDetail[submitData.date].push(submitData);
                        that.data.accountDetail = that.sortBillByDate(that.data.accountDetail);
                        that.setData({
                            accountDetail: that.data.accountDetail
                        });
                        that.calcCurIncomeAndExpand();
                    }
                }
            })
        } else {
            if (that.data.modifyBillId != "") {
                bills.doc(that.data.modifyBillId).update({
                    data: submitData,
                    success: function(res) {
                        that.updateBill(that.data.modifyBillId, submitData);
                    }
                });
            }
        }
        this.setData({
            modalName: null
        })
    },
    
    tabSelect(e) {
        this.setData({
            TabCur: e.currentTarget.dataset.id,
        })
    },

    // 选中某一项
    onSelectItem(e) {
        if (0 == this.data.TabCur) {
            this.setData({
                selectExpandItemIndex: e.currentTarget.dataset.index
            })
        } else {
            this.setData({
                selectIncomeItemIndex: e.currentTarget.dataset.index
            })
        }
    },

    // 金额改变
    onMoneyChange(e) {
        this.setData({
            money: e.detail
        })
    },

    findItemIndex(tab, text) {
        if (0 == tab) {
            for (var idx in this.data.expanditem) {
                if (this.data.expanditem[idx].text == text) {
                    return idx;
                }
            }
        } else {
            for (var idx in this.data.incomeitem) {
                if (this.data.incomeitem[idx].text == text) {
                    return idx;
                }
            }
        }
        return 0;
    },

    // 修改账单
    onModifyBill(e) {
        var billid = e.currentTarget.dataset.billid;
        var find = false;
        var modifyBill;
        for (var datekey in this.data.accountDetail) {
            var valueArr = this.data.accountDetail[datekey];
            for (var index in valueArr) {
                if (billid == valueArr[index]._id) {
                    modifyBill = valueArr[index];
                    find = true;
                    break;
                }
            }
            if (find) break;
        }

        if (modifyBill) {
            let tab = modifyBill.category == "支出" ? 0 :1;
            let itemIndex = this.findItemIndex(tab, modifyBill.itemtext);
            if (0 == this.data.TabCur) {
                this.data.selectExpandItemIndex = itemIndex;
            } else {
                this.data.selectIncomeItemIndex = itemIndex;
            }
            this.setXmove(billid, 0);
            this.setData({
                modalName: "bottomModal",
                TabCur: tab,
                selectExpandItemIndex: this.data.selectExpandItemIndex,
                selectIncomeItemIndex: this.data.selectIncomeItemIndex,
                money: modifyBill.money,
                pickerdate: modifyBill.date,
                note: modifyBill.note,
                buttonText: "修改",
                modifyBillId: modifyBill._id,
            });
        }
    },
    
    // 删除账单
    onDelBill(e) {
        var billid = e.currentTarget.dataset.id;
        var find = false;
        for (var datekey in this.data.accountDetail) {
            var valueArr = this.data.accountDetail[datekey];
            for (var index in valueArr) {
                if (billid == valueArr[index]._id) {
                    valueArr.splice(index, 1);
                    if (0 == valueArr.length) {
                        delete this.data.accountDetail[datekey];
                    }
                    this.setData({
                        accountDetail: this.data.accountDetail
                    });
                    find = true;
                    break;
                }
            }
            if (find) {
                const db = wx.cloud.database();
                const bills = db.collection("bills");
                bills.doc(billid).remove();
                this.calcCurIncomeAndExpand();
            };
        }
    },
    /**
     * 显示删除按钮
     */
    showDeleteButton: function(e) {
        this.setXmove(e.currentTarget.dataset.billid, -180);
    },
    /**
     * 隐藏删除按钮
     */
    hideDeleteButton: function(e) {
        this.setXmove(e.currentTarget.dataset.billid, 0);
    },
    /**
     * 设置movable-view位移
     */
    setXmove: function(billid, xmove) {
        var find = false;
        let actDetail = this.data.accountDetail;
        for (var datekey in actDetail) {
            var valueArr = actDetail[datekey];
            for (var index in valueArr) {
                valueArr[index].xmove = 0;
                if (billid == valueArr[index]._id) {
                    valueArr[index].xmove = xmove;
                }
            }
        }
        this.setData({
            accountDetail: actDetail
        });
    },

    setStartX: function(billid, startX) {
        var find = false;
        let actDetail = this.data.accountDetail;
        for (var datekey in actDetail) {
            var valueArr = actDetail[datekey];
            for (var index in valueArr) {
                if (billid == valueArr[index]._id) {
                    valueArr[index].startX = startX;
                    this.setData({
                        accountDetail: actDetail
                    });
                    find = true;
                    break;
                }
            }
            if (find) break;
        }
    },

    getStartX: function(billid) {
        let actDetail = this.data.accountDetail;
        for (var datekey in actDetail) {
            var valueArr = actDetail[datekey];
            for (var index in valueArr) {
                if (billid == valueArr[index]._id) {
                    return valueArr[index].startX;
                }
            }
        }
        return 0;
    },

    /**
     * 处理movable-view移动事件
     */
    handleMovableChange(e) {
        if (e.detail.source === 'friction') {
            if (e.detail.x < -30) {
                this.showDeleteButton(e)
            } else {
                this.hideDeleteButton(e)
            }
        } else if (e.detail.source === 'out-of-bounds' && e.detail.x === 0) {
            this.hideDeleteButton(e)
        }
    },

    /**
     * 处理touchstart事件
     */
    handleTouchStart(e) {
        this.setStartX(e.currentTarget.dataset.billid, e.touches[0].pageX);
    },

    /**
     * 处理touchend事件
     */
    handleTouchEnd(e) {
        var curStartX = this.getStartX(e.currentTarget.dataset.billid);
        if (e.changedTouches[0].pageX < curStartX && e.changedTouches[0].pageX - curStartX <= -30) {
            this.showDeleteButton(e)
        } else if (e.changedTouches[0].pageX > curStartX && e.changedTouches[0].pageX - curStartX < 10) {
            this.showDeleteButton(e)
        } else {
            this.hideDeleteButton(e)
        }
    },

    /**
     * 计算当月收入和支出
     */
    calcCurIncomeAndExpand() {
        let actDetail = this.data.accountDetail;
        this.data.curMonthIncome = 0;
        this.data.curMonthExpand = 0;
        for (var datekey in actDetail) {
            let valueArr = actDetail[datekey];
            for (var index in valueArr) {
                if ("收入" == valueArr[index].category) {
                    this.data.curMonthIncome += valueArr[index].money;
                } else {
                    this.data.curMonthExpand += valueArr[index].money;
                }
            }
        }

        this.setData({
            curMonthIncome: this.data.curMonthIncome,
            curMonthExpand: this.data.curMonthExpand,
        });
    },
    // 选择账本
    onSelectAccount() {
        this.setData({ show: true });
    },

    onClose() {
        this.setData({ show: false });
    },

    onSelect(event) {
        this.setData({
            accountName: event.detail.name
        });
        this.setData({ show: false });
        this.loadCurMonthBill();
    },
})