// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database({
    env: 'release-8o19d'
})
const $ = db.command.aggregate

// 云函数入口函数
exports.main = async (event, context) => {
    return db.collection("bills").aggregate().group({
        _id: '$category',
        totalMoney: $.sum('$money')
    }).end();
}