// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database({
    env: 'release-8o19d'
})
const _ = db.command;
const $ = db.command.aggregate

// 云函数入口函数
exports.main = async (event, context) => {
    // 获取近6个月的数据
    return db.collection("bills").aggregate().group({
        _id: '$account',
        totalMoney: $.sum('$money')
    }).end();
}